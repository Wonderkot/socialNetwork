PROJ='social-network'
GIT_HASH="$(git rev-parse --short HEAD)"

docker build --tag wonderkot/$PROJ:$GIT_HASH .
docker push wonderkot/$PROJ:$GIT_HASH

docker build --tag wonderkot/$PROJ:latest .
docker push wonderkot/$PROJ:latest
echo "pushed"